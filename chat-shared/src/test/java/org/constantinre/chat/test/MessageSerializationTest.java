package org.constantinre.chat.test;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Date;

import org.constantinre.chat.BufferMessageUtil;
import org.constantinre.chat.ByteArrayMessageUtil;
import org.constantinre.chat.Message;
import org.constantinre.chat.exception.ProtocolException;
import org.junit.Test;

import static org.junit.Assert.*;

public class MessageSerializationTest {

	private void testByteArray(String userName, Date dt, String message) throws IOException, ProtocolException {

		Message m1 = new Message(userName, dt, message);
		
		byte[] buffer = getByteArray(m1);
		
		checkByteArray(m1, buffer);
	}
	
	private void testBuffer(String userName, Date dt, String message) throws ProtocolException {
		Message m1 = new Message(userName, dt, message);
		
		ByteBuffer bb = getByteBuffer(m1);
		
		checkBuffer(m1, bb);
	}
	
	private byte[] getByteArray(Message m1) throws IOException {
		
		byte[] buffer = new byte[ByteArrayMessageUtil.guessByteLength(m1)];
		
		int written = ByteArrayMessageUtil.toBytes(m1, buffer, 0);
		
		assertTrue(written <= buffer.length);
		
		assertEquals(written, ByteArrayMessageUtil.checkByteLength(buffer, 0));
		
		return buffer;
	}
	
	private ByteBuffer getByteBuffer(Message m1) {
		ByteBuffer bb = ByteBuffer.allocate(BufferMessageUtil.guessByteLength(m1));
		
		int written = BufferMessageUtil.toBuffer(m1, bb);
		
		assertTrue(written <= bb.capacity());
		
		bb.rewind();
		
		assertEquals(written, BufferMessageUtil.checkByteLength(bb));
		
		bb.rewind();
		
		return bb;
	}
	
	private void checkByteArray(Message m1, byte[] buffer) throws ProtocolException {

		assertTrue(ByteArrayMessageUtil.checkByteLength(buffer, 0) <= buffer.length);
		
		Message m2 = ByteArrayMessageUtil.fromBytes(buffer, 0);
		
		assertEquals(m1.getUserName(), m2.getUserName());
		assertEquals(m1.getMessageDate(), m2.getMessageDate());
		assertEquals(m1.getMessageText(), m2.getMessageText());
	}
	
	private void checkBuffer(Message m1, ByteBuffer bb) throws ProtocolException {

		assertTrue(BufferMessageUtil.checkByteLength(bb) <= bb.capacity());
		
		bb.rewind();
		
		Message m2 = BufferMessageUtil.fromBuffer(bb);
		
		assertEquals(m1.getUserName(), m2.getUserName());
		assertEquals(m1.getMessageDate(), m2.getMessageDate());
		assertEquals(m1.getMessageText(), m2.getMessageText());
	}
	
	private void testByteArrayToBufferSerialization(String userName, Date dt, String message) throws IOException, ProtocolException {

		Message m1 = new Message(userName, dt, message);
		
		byte[] buffer = getByteArray(m1);
		
		ByteBuffer bb = ByteBuffer.wrap(buffer);
		bb.rewind();
		
		checkBuffer(m1, bb);
	}
	
	private void testBufferToByteArraySerialization(String userName, Date dt, String message) throws ProtocolException {
		Message m1 = new Message(userName, dt, message);
		
		ByteBuffer bb = getByteBuffer(m1);
		
		bb.rewind();
		
		byte[] buffer = new byte[bb.capacity()];
		bb.get(buffer);
		
		checkByteArray(m1, buffer);
	}
	
	@Test
	public void testSerialization() throws IOException, ProtocolException {
		testByteArray("USER_5678", new Date(1234567891), "MESSAGE SERIALIZATION TEST ���� ������������ ���������");
		
	}
	
	@Test
	public void testSerializationLarge() throws IOException, ProtocolException {

		char[] msg = new char[100000];
		Arrays.fill(msg, 'a');
		
		testByteArray("User", new Date(), new String(msg));
	}
	
	@Test
	public void testSerializationBuffer() throws ProtocolException {
		testBuffer("USER_5678", new Date(1234567891), "MESSAGE SERIALIZATION TEST ���� ������������ ���������");
		
	}
	
	@Test
	public void testSerializationBufferLarge() throws ProtocolException {

		char[] msg = new char[100000];
		Arrays.fill(msg, 'a');
		
		testBuffer("User", new Date(), new String(msg));
	}
	
	@Test
	public void testArrayToBuffer() throws ProtocolException, IOException {
		testByteArrayToBufferSerialization("USER_5678", new Date(1234567891), "MESSAGE SERIALIZATION TEST ���� ������������ ���������");
	}
	
	@Test
	public void testBufferToArray() throws IOException, ProtocolException {
		testBufferToByteArraySerialization("USER_5678", new Date(1234567891), "MESSAGE SERIALIZATION TEST ���� ������������ ���������");
	}
	
	@Test
	public void testArrayToBufferLarge() throws IOException, ProtocolException {
		char[] msg = new char[100000];
		Arrays.fill(msg, 'a');
		testByteArrayToBufferSerialization("User", new Date(), new String(msg));
	}
	
	@Test
	public void testBufferToArrayLarge() throws IOException, ProtocolException {
		char[] msg = new char[100000];
		Arrays.fill(msg, 'a');
		testBufferToByteArraySerialization("User", new Date(), new String(msg));		
	}
}
