package org.constantinre.chat;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Date;

import org.constantinre.chat.exception.ProtocolException;

public abstract class BufferMessageUtil extends CommonMessageUtil {
	
	private static Charset ch = Charset.forName("UTF-16LE");
	
	private static final short TYPE_ID = 0x1234;
	
	public static int toBuffer(Message m, ByteBuffer buffer) {
		
		byte[] _userName = m.getUserName().getBytes(ch);
		byte[] _message = m.getMessageText().getBytes(ch);
		
		int length = guessByteLength(m);
		
		buffer.putShort(TYPE_ID);
		buffer.putInt(length);
		buffer.putLong(m.getMessageDate().getTime());
		buffer.putInt(_userName.length);
		buffer.put(_userName);
		buffer.putInt(_message.length);
		buffer.put(_message);		
		
		return length;
	}
		
	public static int checkByteLength(ByteBuffer buffer) {
		if (buffer.remaining() < 6 || buffer.getShort() != TYPE_ID)
			return -1; 
		else
			return buffer.getInt();
	}
	
	public static Message fromBuffer(ByteBuffer buffer) throws ProtocolException {
		
		Message result = new Message(null, null, null);
		
		if (buffer.getShort() != TYPE_ID)
			throw new ProtocolException("Invalid TYPE_ID");
		
		int length = buffer.getInt(); //do nothing with message length
		
		if (length < MIN_MESSAGE_LENGTH) {
			throw new ProtocolException(String.format("Message length %s less than minimum %s.", length, MIN_MESSAGE_LENGTH));
		}
		
		result.setMessageDate(new Date(buffer.getLong()));
		
		Charset ch = Charset.forName("UTF-16LE");
		
		byte[] _message = new byte[buffer.getInt()];		
		buffer.get(_message);
		result.setUserName(new String(_message, ch));
		
		_message = new byte[buffer.getInt()];
		buffer.get(_message);		
		result.setMessageText(new String(_message, ch));
		
		return result;
	}
	
	public static String getMessageHeader(ByteBuffer bb) {
		return String.format("type:0x%02x%02x length:0x%02x%02x%02x%02x", bb.get(), bb.get(), bb.get(), bb.get(), bb.get(), bb.get());
	}
	
}
