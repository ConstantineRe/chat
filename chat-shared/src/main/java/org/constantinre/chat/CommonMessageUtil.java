package org.constantinre.chat;

public abstract class CommonMessageUtil {
	
	public static final int MIN_MESSAGE_LENGTH = 22;

	//����������� ������������ ����� ��������� � ������
	//�������� ������ � UTF-16, ������ ������ �� 2 �� 4 ����
	public static int guessByteLength(Message m) {
		return 2 + 4 + 8 + 4 + m.getMessageText().length() * 2 + 4 + m.getUserName().length() * 2;
	}
	
}
