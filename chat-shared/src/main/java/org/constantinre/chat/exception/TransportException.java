package org.constantinre.chat.exception;

/**
 * ������, �� ������������ ������� ������ �������.
 * 
 * @author Constantin
 *
 */
public class TransportException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransportException(String message) {
		super(message);
	}
	
	public TransportException(String message, Exception e) {
		super(message, e);
	}
	
}
