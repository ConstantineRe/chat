package org.constantinre.chat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;

import org.constantinre.chat.exception.ProtocolException;

public abstract class ByteArrayMessageUtil extends CommonMessageUtil {
	
	private static Charset ch = Charset.forName("UTF-16LE");
	
	private static final short TYPE_ID = 0x1234;
	
	public static int toBytes(Message m, byte[] destination, int offset) throws IOException {
		
		byte[] _userName = m.getUserName().getBytes(ch);
		byte[] _message = m.getMessageText().getBytes(ch);
		
		int length = guessByteLength(m);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream(length);
		DataOutputStream dos = new DataOutputStream(baos);
		dos.writeShort(TYPE_ID);
		dos.writeInt(length);
		dos.writeLong(m.getMessageDate().getTime());
		dos.writeInt(_userName.length);
		dos.write(_userName);
		dos.writeInt(_message.length);
		dos.write(_message);
		dos.flush();
		
		byte[] src = baos.toByteArray();
		System.arraycopy(src, 0, destination, offset, src.length);
		
		return length;
	}
		
	public static int checkByteLength(byte[] source, int offset) {
		if (offset + 6 > source.length || ((source[0] << 8 & 0x0000ff00) + (source[1] & 0x000000ff)) != TYPE_ID)
			return -1; 
		else
			return (source[2] << 24 & 0xff000000) + (source[3] << 16 & 0x00ff0000) + (source[4] << 8 & 0x0000ff00) + (source[5] & 0x000000ff);
	}
	
	public static Message fromBytes(byte[] source, int offset) throws ProtocolException {
		
		Message result = new Message(null, null, null);
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(source, offset, source.length));
		
		try {
			if (dis.readShort() != TYPE_ID)
				throw new ProtocolException("Invalid TYPE_ID");
			
			int length = dis.readInt();
			
			if (length < MIN_MESSAGE_LENGTH) {
				throw new ProtocolException(String.format("Message length %s less than minimum %s.", length, MIN_MESSAGE_LENGTH));
			}
			
			result.setMessageDate(new Date(dis.readLong()));
			
			Charset ch = Charset.forName("UTF-16LE");
			
			byte[] _message = new byte[dis.readInt()];		
			dis.read(_message);		
			result.setUserName(new String(_message, ch));
			
			_message = new byte[dis.readInt()];
			dis.read(_message);		
			result.setMessageText(new String(_message, ch));
		
			return result;

		} catch (IOException e) {
			throw new ProtocolException("Exception while deserializing message. Header - " + getMessageHeader(source, offset));
		}
	}
	
	public static String getMessageHeader(byte[] source, int offset) {
		return String.format("type:0x%02x%02x length:0x%02x%02x%02x%02x", source[offset+0], source[offset+1], source[offset+2], source[offset+3], source[offset+4], source[offset+5]);
	}
	
}
