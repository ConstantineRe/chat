package org.constantinre.chat;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userName;
	
	public void setUserName(String userName) {
		this.userName = userName;
	}

	private Date messageDate;
	
	public void setMessageDate(Date messageDate) {
		this.messageDate = messageDate;
	}

	private String messageText;
	
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	private long sequence;

	public Message(String userName, Date messageDate, String messageText) {
		super();
		this.userName = userName;
		this.messageDate = messageDate;
		this.messageText = messageText;
	}
	
	public String getUserName() {
		return userName;
	}

	public Date getMessageDate() {
		return messageDate;
	}

	public String getMessageText() {
		return messageText;
	}

	public long getSequence() {
		return sequence;
	}

	public void setSequence(long sequence) {
		this.sequence = sequence;
	}
}

