package org.constantinre.chat.client;

import org.constantinre.chat.BufferMessageUtil;
import org.constantinre.chat.Message;
import org.constantinre.chat.exception.ProtocolException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * ������ ����.
 * 
 * @author Constantin
 *
 */
public class ApplicationClient {
	
	private static Logger log = Logger.getLogger(ApplicationClient.class.getName());

	private static Future<?> task = null; 
	
	public static void main(String[] args) {
		
		String path = System.getProperty("application.properties");
		Properties props = new Properties();
		
		final DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

		final int port;
		final String host;
		final int bufferSize;
		 
		try {
			props.load(ApplicationClient.class.getClassLoader().getResourceAsStream(path != null ? path : "application.properties"));

			port = Integer.parseInt(props.getProperty("chat.port"));
			host = props.getProperty("chat.host");
			bufferSize = Integer.parseInt(props.getProperty("chat.buffer.size"));
		} catch (Exception e2) {
			System.err.println("Error while loading properties " + e2);
			e2.printStackTrace();
			return;
		}

		try (SocketChannel channel = SocketChannel.open(new InetSocketAddress(host, port))) {

			System.out.println("Welcome to chat client program.\n"
					+ "Connection to " + host + ":" + port + " established.");
			
			System.out.println("\nType '/quit' command to quit chat client.\n"
					+ "Type '/changeName' command to change you're user name.\n"
					+ "Type '/help' command to get help.\n"
					+ "Type '/commandList' command to view chat commands.");

			ByteBuffer readBuffer = ByteBuffer.allocate(bufferSize);

			task = executorService.scheduleWithFixedDelay(() -> {
                try {
                    channel.read(readBuffer);
                    readBuffer.flip();

                    while (BufferMessageUtil.checkByteLength(readBuffer.asReadOnlyBuffer()) > 0 &&
                            BufferMessageUtil.checkByteLength(readBuffer.asReadOnlyBuffer()) <= readBuffer.remaining()) {
                        Message m = BufferMessageUtil.fromBuffer(readBuffer);
                        System.out.println(m.getUserName() + " (" + df.format(m.getMessageDate()) + "): " + m.getMessageText());
                    }

                    readBuffer.compact();

                } catch (SocketTimeoutException e) {
                    // do nothing?

                } catch (ProtocolException | IOException e) {
                    log.severe("Exception while read message " + e);
                    e.printStackTrace();
                    cancelTask();
                }
            }, 0, 1, TimeUnit.MILLISECONDS);
			
			Scanner scanner = new Scanner(System.in);

			String line;
			boolean quit;
			ByteBuffer writeBuffer = ByteBuffer.allocate(bufferSize);
			
			do {				
				line = scanner.nextLine();
				quit = (line.toLowerCase().startsWith("/quit"));
				
				Message m = new Message("User", new Date(), line);

				BufferMessageUtil.toBuffer(m, writeBuffer);
				writeBuffer.flip();

				channel.write(writeBuffer);
				writeBuffer.compact();

			} while (!quit);						

			cancelTask();
			
		} catch (IOException /*| ClassNotFoundException*/ e) {
			log.severe("Unexpected exception " + e);
			e.printStackTrace();		
		} finally {
			executorService.shutdown();
		}

	}
	
	private static void cancelTask() {
		log.info("Stop reading messages");
		task.cancel(false);
	}

}
