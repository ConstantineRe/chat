package org.constantinre.chat.bot;

import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.constantinre.chat.Message;
import org.constantinre.chat.exception.ProtocolException;
import org.constantinre.chat.exception.TransportException;
import org.constantinre.chat.io.stream.StreamMessageReader;
import org.constantinre.chat.io.stream.StreamMessageWriter;

/**
 * Бот для выполнения нагрузочного тетсирования.
 * Использует словарь для составления случайных фраз. Добавляет новые слова в словарь.
 * Перед сообщением ждет от 5 до 30 секунд.
 * Набирает сообщение со скоростью 180 символов в минуту.
 * 
 * Подсчитывает сообщение, который сам отправил, но не получил.
 * Для этого, он определяет свое имя, назначенное сервером, по служебному сообщению.
 * 
 * @author Constantin
 *
 */
public class Bot {
	
	private static final Logger log = Logger.getLogger(Bot.class.getName());
	
	private boolean stop;
	private Vocabulary voc;
	private Random rnd = new Random();
	private String name = "NO_USER";
	private Set<String> msgSet = new HashSet<>();
	private Statistics stats;

	private long lastType = 0;

	private long counterThink = 0;

	private long counterWrite = 0;
	
	public Bot(Vocabulary voc, Statistics stats) {
		this.stop = true;	
		this.voc = voc;
		this.stats = stats;
	}
	
	public void start() {
		
		log.info("Bot start");
		this.stop = false;
		
		try (Socket s = new Socket("localhost", 11111)) {
			
			s.setSoTimeout(100);
			
			StreamMessageReader mr = new StreamMessageReader(s.getInputStream(), 4096);
			StreamMessageWriter mw = new StreamMessageWriter(s.getOutputStream(), 4096);
			
			while (!stop) {
				try {
					try {
						int count = mr.read();
						log.finer("Bot " + name + " read " + count + " messages");
						
					} catch (TransportException e) {
						if (!(e.getCause() instanceof SocketTimeoutException)) {
							throw e;
						}
					}
				} catch (TransportException | ProtocolException e) {
					log.severe("Exception while read messages: " + e);
					e.printStackTrace();
				}
				while (mr.getMessageCount() > 0) {
						Message m = mr.poll();
										
						//log.info(m.getUserName() + " :: " + m.getMessageText());
						
						if ("Service".equals(m.getUserName())) {
							processServiceMessage(m.getMessageText());
							
						} else if (name.equals(m.getUserName())) {	
							log.info(":: got one");
							msgSet.remove(m.getMessageText());							
							
						} else {
							//запоминаем новые слова
							String[] wrds = m.getMessageText().split("\\s+");
							voc.store(wrds);
						}
					
				}
				
				//сколько прошло времени с последнего сообщений
				Date now = new Date();
				if (now.getTime() - lastType <= counterThink + counterWrite) continue;
				
				lastType = now.getTime();
				
				String msgText = voc.randomSentence();
				
				//задержка на подумать от 5 до 30 секунд
				counterThink = 1000 * (rnd.nextInt(25) + 5);
				//задержка при наборе текста - печать 180 символов в минуту
				counterWrite = Math.round(((msgText.length() / 180) * 60000));
				
				Message m = new Message("User1", new Date(), msgText);
				mw.push(m);				
				mw.write();
				
				log.info("Bot " + name + " write message");
				
				msgSet.add(m.getMessageText());
			}
			
		} catch (Throwable e) {
			log.severe("Unrecognized error while chat " + e);
			e.printStackTrace();
		}
		
	}
	
	public void stop() {
		stop = true;
		
		log.info("Bot stop. " + msgSet.size() + " messages lost.");		
		
		stats.incBotCount(1); 
		stats.incLostMessageCount(msgSet.size());
	}
	
	private void processServiceMessage(String message) {
		if (message.matches("^Info:\\s+you're\\s+username.*'[a-zA-Z0-9_]+'.*")) {
			Matcher m = Pattern.compile("'[a-zA-Z0-9_]+'").matcher(message);
			m.find();
			
			name = m.group().substring(1, m.group().length() - 1);
			
			log.info("Bot got username '" + name + "'");
		}
	}

}
