package org.constantinre.chat.bot;

/**
 * Подсчет статистики ботом.
 * Потокобезопасно.
 * 
 * @author Constantin
 *
 */
public class Statistics {
	
	private int botCount = 0;
	private int lostMessageCount = 0;
	
	public int getBotCount() {
		return botCount;
	}
	
	public int getLostMessageCount() {
		return lostMessageCount;
	}
	
	public synchronized void incBotCount(int value) {
		botCount += value;
	}
	
	public synchronized void incLostMessageCount(int value) {
		lostMessageCount += value;
	}

}
