package org.constantinre.chat.bot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Выполнение нагрузочного тестирования.
 * 
 * 
 * @author Constantin
 *
 */
public class Application {

	private static Logger log = Logger.getLogger(Application.class.getName());
	
	public static void main(String[] args) {
		
		Vocabulary voc = new Vocabulary();
		Map<Bot, Future<?>> schedules = new HashMap<>();
			
		ExecutorService executorService = Executors.newFixedThreadPool(200);
		Statistics stats = new Statistics();
		
		for (int i = 0; i < 200; i++) {
			final Bot b = new Bot(voc, stats);
			
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Future<?> task = executorService.submit(new Runnable() {

				@Override
				public void run() {
					b.start();
				}
				
			});
			
			schedules.put(b, task);
		}
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line;
		try {
			while ((line = br.readLine()) == null || !"quit".equals(line)) {
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		executorService.shutdown();
		
		for (Entry<Bot, Future<?>> schedule : schedules.entrySet()) {
			schedule.getKey().stop();
			schedule.getValue().cancel(false);
		}
		
		schedules.clear();
		
		try {
			executorService.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.info("Statistics - bots:" + stats.getBotCount() 
			+ "; lost:" + stats.getLostMessageCount() 
			+ "; averageLost:" + (1.0 * stats.getLostMessageCount()) / stats.getBotCount());

	}

}
