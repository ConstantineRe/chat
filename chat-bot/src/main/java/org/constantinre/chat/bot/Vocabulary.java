package org.constantinre.chat.bot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Словарь для составления случайных сообщений ботом. Потокобезопасен. Дополняемый.
 * 
 * @author Constantin
 *
 */
public class Vocabulary {
	
	private static String[] voc = {"привет", "пока", "добрый", "день", "спокойной", "ночи",
			"кино", "театр", "пойдем", "да", "нет", "авто", "программа", "работа",
			"тестовое", "задание", "лень", "тест", "так", "ты", "знал", "что", "этих",
			"двоих", "еще", "три", "ясно", "не", "могу", "заключать", "хаос", "война"};

	final Set<String> words = new HashSet<>(Arrays.asList(voc));;
	final ReentrantReadWriteLock wordsLock = new ReentrantReadWriteLock();
	final Random rnd = new Random();
	final String[][] words2 = new String[][] { voc };
	
	public void store(String[] wrds) {
		List<String> newWrds = new ArrayList<>();
		
		wordsLock.readLock().lock();
		try {
			for (String word : wrds) {
				if (!words.contains(word)) newWrds.add(word);
			}
		} finally {
			wordsLock.readLock().unlock();
		}
		
		if (newWrds.size() > 0) {
			wordsLock.writeLock().lock();
			try {
				words.addAll(newWrds);
				words2[0] = words.toArray(new String[words.size()]);
			} finally {
				wordsLock.writeLock().unlock();
			}
		}
		
	}
	
	public String randomSentence() {
		
		//Составляем рандомную фразу
		wordsLock.readLock().lock();
		StringBuilder sb = new StringBuilder();
		try {
			for (int i = 0; i < rnd.nextInt(10); i++) {
				sb.append(words2[0][rnd.nextInt(words2[0].length)]);
				sb.append(" ");
			}
		} finally {
			wordsLock.readLock().unlock();
		}
		
		return sb.toString();
	}
}
