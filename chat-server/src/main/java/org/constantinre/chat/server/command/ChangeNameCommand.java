package org.constantinre.chat.server.command;

import org.constantinre.chat.server.Application;
import org.constantinre.chat.server.BaseUserCommand;
import org.constantinre.chat.server.UserData;

public class ChangeNameCommand extends BaseUserCommand {
	
	public ChangeNameCommand() {
		super("Change name", "Change you name", "/changeName");
	}
	
	@Override
	public void handleCommand(UserData uc, Application application, String[] args) {
		if (args.length < 2) {
			application.sendMessage(uc, "Error: 'name' parameter is missing.");
		} else {
			//UserRegistration registration = context.getRegistry().checkRegistration(uc);
			
			if (uc.getUserName().equals(args[1])) {
				application.sendMessage(uc, "Info: you're username is '" + args[1] + "' already.");
			
			} else if (!args[1].matches("^[a-zA-Z0-9_]+$")) {
				application.sendMessage(uc, "Error: invalid username pattern. use (^[a-zA-Z0-9_]+$).");
				
			} else {
				uc.setUserName(args[1]);
				application.sendMessage(uc, "Success: you're username is " + args[1] + " now");
			}
		}
		
	}
	
}
