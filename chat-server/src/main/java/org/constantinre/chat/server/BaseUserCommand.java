package org.constantinre.chat.server;

import org.constantinre.chat.server.api.UserCommand;

/**
 * ����������� ���������� ������� ������������.
 * ���������� ���������� ��-����������.
 * 
 * @author Constantin
 *
 */
public abstract class BaseUserCommand implements UserCommand {

	private String name;
	private String description;
	private String pattern;

	public BaseUserCommand(String name, String description, String pattern) {
		super();
		
		this.name = name;
		this.description = description;
		this.pattern = pattern;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getPattern() {
		return pattern;
	}

	@Override
	public abstract void handleCommand(UserData user, Application application, String[] args);

}
