package org.constantinre.chat.server.command;

import org.constantinre.chat.server.Application;
import org.constantinre.chat.server.BaseUserCommand;
import org.constantinre.chat.server.UserData;

public class RegisterAdminCommand extends BaseUserCommand {
	
	public RegisterAdminCommand() {
		super("Register admin", "Register user as admin", "/registerAdmin");
	}
	
	@Override
	public void handleCommand(UserData uc, Application application, String[] args) {
		
		if (args.length <= 2) {
			application.sendMessage(uc, "Type password as first command agument.");
			return;
		}
		
//		if (context.getRegistry().isAdmin(uc)) {
//			application.sendMessage(uc, "Info: you're admin already.");
//			return;
//		}
		
//		try {
//			context.getRegistry().registerAdmin(uc, args[1]);
			
			application.sendMessage(uc, "User " + uc + " is Admin now.");
			
//		} catch (AccessException e) {
//			application.sendMessage(uc, "Error: " + e.getMessage());
//		}
	}

}
