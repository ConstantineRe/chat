package org.constantinre.chat.server.exception;

public class MessageTooOldException extends Exception {

    public MessageTooOldException(String message) {
        super(message);
    }

}
