package org.constantinre.chat.server;

import org.constantinre.chat.Message;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Queue;

/**
 * ������ � ������������ ������������.
 * 
 * @author Constantin
 *
 */
public class UserData {
	
	private String userName;

	private long lastWriteMessage;

	private ByteBuffer readBuffer;

	private ByteBuffer writeBuffer;

	private SocketChannel channel;

	private Queue<Message> messageQueue;

	public UserData() {
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getLastWriteMessage() {
		return lastWriteMessage;
	}

	public void setLastWriteMessage(long lastWriteMessage) {
		this.lastWriteMessage = lastWriteMessage;
	}

	public ByteBuffer getReadBuffer() {
		return readBuffer;
	}

	public void setReadBuffer(ByteBuffer readBuffer) {
		this.readBuffer = readBuffer;
	}

	public ByteBuffer getWriteBuffer() {
		return writeBuffer;
	}

	public void setWriteBuffer(ByteBuffer writeBuffer) {
		this.writeBuffer = writeBuffer;
	}

	public SocketChannel getChannel() {
		return channel;
	}

	public void setChannel(SocketChannel channel) {
		this.channel = channel;
	}

	public Queue<Message> getMessageQueue() {
		return messageQueue;
	}

	public void setMessageQueue(Queue<Message> messageQueue) {
		this.messageQueue = messageQueue;
	}

}
