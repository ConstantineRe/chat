package org.constantinre.chat.server.command;

import org.constantinre.chat.server.Application;
import org.constantinre.chat.server.BaseUserCommand;
import org.constantinre.chat.server.UserData;

public class TestCommand extends BaseUserCommand {
	
	public TestCommand() {
		super("Test commands", "Test commands and arguments", "/test");
	}
	
	@Override
	public void handleCommand(UserData uc, Application application, String[] args) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Testing commands. Arguments:\n");
		for (String arg : args) {
			sb.append("[");
			sb.append(arg);
			sb.append("]\n");
		}
		sb.append("End.");
		
		application.sendMessage(uc, sb.toString());
		 
	}	

}
