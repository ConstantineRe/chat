package org.constantinre.chat.server.exception;

public class AccessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccessException(String format) {
		super(format);
	}

}
