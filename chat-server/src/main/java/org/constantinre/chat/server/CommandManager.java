package org.constantinre.chat.server;

import org.constantinre.chat.server.api.UserCommand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 * �������� ���������� ��������� ����.
 */
public class CommandManager {
	
	private static Logger log = Logger.getLogger(CommandManager.class.getName());
	private final Application application;

	private List<UserCommand> userCommandList;
	
	public CommandManager(Application application, UserCommand[] commands, String commandClassList) {
		
		if (application == null) {
			throw new NullPointerException("application is null");
		}
		
		this.application = application;
		
		this.userCommandList = new ArrayList<>();
		
		for (UserCommand uc : commands) {
			addCommand(uc);
		}
		
		if (commandClassList != null && !"".equals(commandClassList)) {
			loadCommandList(commandClassList);
		}
	}

	public boolean isCommand(String text) {
		return text != null && text.startsWith("/"); 
	}

	public void handleCommand(UserData user, String text) {
		
		List<UserCommand> userCommandList = matchUserCommands(text);
		
		if (userCommandList.size() == 0) {
			application.sendMessage(user, String.format("Command \"%s\" not found", text));
		
		} else if (userCommandList.size() == 1) {
			log.fine(String.format("Execute [%s] command for user %s", text, user));
			
			String args[] = text.split("[\\s]+");
			userCommandList.get(0).handleCommand(user, application, args);
			
		} else {				
			application.sendMessage(user, getUserCommandListMessages(userCommandList));
		}
		
	}
	
	private void loadCommandList(String classList) {

		String[] classes = classList.split(";");
		
		for (String cls : classes) {
			try {
				Class<?> cl = this.getClass().getClassLoader().loadClass(cls);
				
				Class<? extends UserCommand> ucl = cl.asSubclass(UserCommand.class);
				
				UserCommand uc = ucl.newInstance();
				
				addCommand(uc);
				
			} catch (ClassCastException e) {
				log.warning("Class " + cls + " is not a UserCommand subclass: " + e);
				e.printStackTrace();				
			} catch (ClassNotFoundException e) {
				log.warning("Command class " + cls + " not found: " + e);
				e.printStackTrace();
			} catch (InstantiationException | IllegalAccessException e) {
				log.warning("Exception while create instance of " + cls + " command class: " + e);
				e.printStackTrace();
			}
		}
		
	}
	
	private List<UserCommand> matchUserCommands(String text) {
		
		if (text == null) {
			return Collections.emptyList();
		}
		
		if (!text.startsWith("/")) {
			return Collections.emptyList();
		} 
		
		List<UserCommand> result = new ArrayList<>();
		
		String cmd = text.split("\\s+")[0];
	
		for (UserCommand uc : userCommandList) {
			if (uc.getPattern().startsWith(cmd)) {				
				if (uc.getPattern().equals(cmd)) {
					return Arrays.asList(uc);
				}
				
				result.add(uc);
			}
		}	
		
		return result;
	}
	
	private String getUserCommandListMessages(List<UserCommand> userCommandList) {
		
		StringBuilder sb = new StringBuilder();		
		
		sb.append("List found " + userCommandList.size() + " user commands:\n");
		
		int i = 1;
		for (UserCommand uc : userCommandList) {
			sb.append((i++) + ": " + uc.getName() + " {" + uc.getPattern() + "} - " + uc.getDescription() + "\n");
		}
		
		sb.append("End user commands list.");
		
		return sb.toString();
	}

	public void addCommand(UserCommand command) {
		if (command == null) {
			throw new NullPointerException("Command is null");
		}
		
		if (userCommandList.indexOf(command) < 0) {
			userCommandList.add(command);
			
			log.info("Load command " + command.getName() + " [" + command.getPattern() + "]" );
		}
	}

	public List<UserCommand> getUserCommandList() {
		return userCommandList;
	}

}
