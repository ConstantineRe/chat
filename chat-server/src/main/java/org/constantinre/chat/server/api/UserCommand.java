package org.constantinre.chat.server.api;

import org.constantinre.chat.server.Application;
import org.constantinre.chat.server.UserData;

/**
 * ������� ������������.
 * ����� ��������, ��������, ����� ������� � ����������.
 * 
 * @author Constantin
 *
 */
public interface UserCommand {
	
	String getName();
	
	String getDescription();
	
	/**
	 * ����� ������� �������. 
	 * 
	 * @return
	 */
	String getPattern();

	/**
	 * ���������� �������.
	 * 
	 * @param uc - ����������� ������������, ���������� �������.
	 * @param args - ���������, ���������� ������ � ��������.
	 */
	void handleCommand(UserData user, Application application, String[] args);

}
