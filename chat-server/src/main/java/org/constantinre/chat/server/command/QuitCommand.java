package org.constantinre.chat.server.command;

import org.constantinre.chat.server.Application;
import org.constantinre.chat.server.BaseUserCommand;
import org.constantinre.chat.server.UserData;

public class QuitCommand extends BaseUserCommand {

	public QuitCommand() {
		super("Quit command", "Disconnect user", "/quit");
	}
	
	@Override
	public void handleCommand(UserData uc, Application application, String[] args) {
		application.disconnect(uc);
	}	

}
