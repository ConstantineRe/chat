package org.constantinre.chat.server.command;

import org.constantinre.chat.server.Application;
import org.constantinre.chat.server.BaseUserCommand;
import org.constantinre.chat.server.UserData;
import org.constantinre.chat.server.api.UserCommand;

public class CommandListCommand extends BaseUserCommand {
	
	public CommandListCommand() {
		super("Commands List", "Get Commands Information", "/commandList");
	}
	
	@Override
	public void handleCommand(UserData uc, Application application, String[] args) {
		
		StringBuilder sb = new StringBuilder("Commands List:\n");
		
		for (UserCommand c : application.getCommandManager().getUserCommandList()) {
			sb.append(c.getPattern());
			sb.append(" - ");
			sb.append(c.getDescription());
			sb.append("\n");
		}
		
		sb.append("End List");
		
		application.sendMessage(uc, sb.toString());
		
	}
}
