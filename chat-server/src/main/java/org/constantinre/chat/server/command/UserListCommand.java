package org.constantinre.chat.server.command;

import java.util.List;

import org.constantinre.chat.server.Application;
import org.constantinre.chat.server.BaseUserCommand;
import org.constantinre.chat.server.UserData;

public class UserListCommand extends BaseUserCommand {
	
	public UserListCommand() {
		super("List users", "Print user list", "/userList");
	}
	
	@Override
	public void handleCommand(UserData uc, Application application, String[] args) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Print user list:\n");
		
		List<UserData> userDataList = application.getUsers();
		
		for (int i = 0; i < userDataList.size(); i++) {
			sb.append((i + 1));
			sb.append(": ");
			sb.append(userDataList.get(i).getUserName());
			sb.append("\n");
		}
		
		sb.append(userDataList.size() + " total.\n");
		sb.append("End user list");
		
		application.sendMessage(uc, sb.toString());
	}	 

}
