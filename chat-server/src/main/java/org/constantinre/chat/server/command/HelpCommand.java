package org.constantinre.chat.server.command;

import org.constantinre.chat.server.Application;
import org.constantinre.chat.server.BaseUserCommand;
import org.constantinre.chat.server.UserData;

public class HelpCommand extends BaseUserCommand {
	
	public HelpCommand() {
		super("Help", "Get help", "/help");
	}
	
	@Override
	public void handleCommand(UserData uc, Application application, String[] args) {
		
		application.sendMessage(uc, "Type '/commandList' to get commands information.");
		
	}

}
