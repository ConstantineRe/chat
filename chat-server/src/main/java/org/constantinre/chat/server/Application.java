package org.constantinre.chat.server;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.constantinre.chat.BufferMessageUtil;
import org.constantinre.chat.Message;
import org.constantinre.chat.exception.ProtocolException;
import org.constantinre.chat.server.api.UserCommand;
import org.constantinre.chat.server.exception.MessageTooOldException;

/**
 * ������ �������.
 * 
 * @author Constantin
 *
 */
public class Application {
	
	private static Logger log = Logger.getLogger(Application.class.getName());

	private int messageQueueSize;

	private int port;

	private int chatMessageQueueTail;

	private Selector selector;

	private ServerSocketChannel server;

	private long messageSeq;

	private MessagesBank messageBank;

	private CommandManager commandManager;

	private String commandClassList;

	private int bufferSize;

	private Application() {
		loadProperties();
		init();
	}
	
	public static void main(String[] args) {
		new Application().run();
	}
	
	public void run() {
		log.info("Server start");

		while (true) {
			try {
				process();
			} catch (IOException e) {
				log.warning("IOException " + e.getMessage() + " while running");
			}
		}
	}

	public void sendMessage(UserData user, String text) {
		Message msg = new Message("System", new Date(), text);
		user.getMessageQueue().add(msg);
	}

	public CommandManager getCommandManager() {
		return commandManager;
	}

	public List<UserData> getUsers() {
		return selector.keys().stream()
				.map(k -> (UserData) k.attachment())
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

	public void disconnect(UserData userData) {
		try {
			selector.selectedKeys().stream()
					.filter(k -> k.attachment() == userData)
					.forEach(SelectionKey::cancel);

			userData.getChannel().close();

		} catch (IOException e) {
			log.severe("Error on disconnect user " + userData.getUserName());
		}
	}

	private void process() throws IOException {
		selector.select();
		for (SelectionKey key : selector.selectedKeys()) {
			if (messageBank.isFull()) {
				compactMessages();
			}

			UserData userData = (UserData) key.attachment();
			try {
				if (key.isAcceptable()) {
					accept(key);
				}

			} catch (IOException e) {
				log.warning("IOException " + e.getMessage() + " while accept connection");
			}

			try {
				if (key.isReadable()) {
					read(key, userData);
				}

				if (key.isWritable()) {
					write(key, userData);
				}
			} catch (CancelledKeyException e) {
				log.info("User " + userData.getUserName() + " was cancelled");

			} catch (IOException | ProtocolException e) {
				log.warning(e.getClass().getSimpleName() + " " + e.getMessage() + " while process user " + userData.getUserName() + ". User will be disconnected.");
				key.cancel();
				userData.getChannel().close();
			} catch (MessageTooOldException e) {
				log.warning("User " + userData.getUserName() + " lost message history and will be disconnected.");
				key.cancel();
				userData.getChannel().close();
			}

		}
		selector.selectedKeys().clear();
	}

	private void accept(SelectionKey key) throws IOException {
		ServerSocketChannel server = (ServerSocketChannel) key.channel();
		SocketChannel channel = server.accept();
		channel.configureBlocking(false);

		UserData data = new UserData();
		data.setUserName("Anonymous");
		data.setReadBuffer(ByteBuffer.allocate(bufferSize));
		data.setWriteBuffer(ByteBuffer.allocate(bufferSize));
		data.getWriteBuffer().flip();
		data.setChannel(channel);
		data.setMessageQueue(new LinkedList<>());

		// calculate last message
		if (messageBank.isEmpty()) {
			data.setLastWriteMessage(messageSeq);
		} else if (messageBank.size() <= chatMessageQueueTail) {
			data.setLastWriteMessage(messageBank.get(0).getSequence() - 1);
		} else {
			data.setLastWriteMessage(messageBank.get(messageBank.size() - chatMessageQueueTail).getSequence() - 1);
		}

		SelectionKey clientKey = channel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
		clientKey.attach(data);

		log.info("Accept connection from " + channel.getRemoteAddress());
	}

	private void read(SelectionKey key, UserData data) throws IOException, ProtocolException {
		ByteBuffer readBuffer = data.getReadBuffer();
		SocketChannel channel = data.getChannel();
		channel.read(readBuffer);
		readBuffer.flip();

		ByteBuffer tempBuffer = readBuffer.asReadOnlyBuffer();
		int size = BufferMessageUtil.checkByteLength(tempBuffer);

		if (size > readBuffer.capacity()) {
			throw new ProtocolException("User " + data.getUserName() + " message size " + size + " more than buffer size " + readBuffer.capacity());
		}

		if (size > 0 && readBuffer.remaining() >= size) {
			Message msg = BufferMessageUtil.fromBuffer(readBuffer);

			if (commandManager.isCommand(msg.getMessageText())) {
				commandManager.handleCommand(data, msg.getMessageText());

			} else {
				msg.setUserName(data.getUserName());
				msg.setMessageDate(new Date());
				msg.setSequence(++messageSeq);
				messageBank.push(msg);
			}

			log.finer("Read message " + msg.getMessageText());
		}
		readBuffer.compact();
	}

	private void compactMessages() {
		log.info("Compacting messages (" + messageBank.size() + ")");

		long minKnownMessage = getUsers().stream()
				.mapToLong(UserData::getLastWriteMessage)
				.min()
				.orElse(-1);

		int found = messageBank.searchBySequence(minKnownMessage + 1);

		if (found < 0) {
			// next message for slowest user not found = some users too slow
			log.warning("Some users lost message flow. kick them all!");
			long lastMessageSeq = messageBank.get(0).getSequence();
			selector.keys().forEach(k -> {
				if (k.attachment() == null) {
					return;
				}
				UserData ud = (UserData) k.attachment();
				try {
					if (ud.getLastWriteMessage() + 1 < lastMessageSeq) {
						log.warning("Kick user " + ud.getUserName());
						k.cancel();
						ud.getChannel().close();
					}
				} catch (IOException e) {
					log.severe("Error " + e.getMessage() + " while disconnect user " + ud.getUserName());
				}
			});

		} else {
			try {
				messageBank.compactMessages(minKnownMessage);
			} catch (MessageTooOldException e) {
				// this exception must not be
				log.severe("Error " + e.getMessage() + "  while compacting messages");
			}
		}

		log.fine("Compacting messages complete (" + messageBank.size() + ") ");
	}

	private void write(SelectionKey key, UserData data) throws IOException, MessageTooOldException, ProtocolException {
		final ByteBuffer writeBuffer = data.getWriteBuffer();
		final SocketChannel channel = (SocketChannel) key.channel();
		channel.write(writeBuffer);
		writeBuffer.compact();

		int free = writeBuffer.limit() - writeBuffer.position();

		Message nextMessage;

		if (!data.getMessageQueue().isEmpty()) {
			nextMessage = data.getMessageQueue().poll();
		} else {
			nextMessage = findNextMessage(data.getLastWriteMessage());
			if (nextMessage != null) {
				data.setLastWriteMessage(nextMessage.getSequence());
			}
		}

		if (nextMessage != null) {
			int nextMessageSize = BufferMessageUtil.guessByteLength(nextMessage);
			if (nextMessageSize > writeBuffer.capacity()) {
				throw new ProtocolException("User " + data.getUserName() + " message size " + nextMessageSize + " more than buffer size " + writeBuffer.capacity());
			}
			if (nextMessageSize <= free) {
				BufferMessageUtil.toBuffer(nextMessage, writeBuffer);

				log.finer("Write message " + nextMessage.getMessageText());
			}
		}

		writeBuffer.flip();
	}

	private Message findNextMessage(long lastWriteMessage) throws MessageTooOldException {

		if (messageBank.isEmpty()) {
			return null;
		}

		int found = messageBank.searchBySequence(lastWriteMessage + 1);

		if (found == -1) {
			throw new MessageTooOldException("Next message seems to be too old");
		}

		if (found < 0) {
			return null;
		}

		return messageBank.get(found);
	}

	private void loadProperties() {
		
		String path = System.getProperty("application.properties");
		
		Properties props = new Properties();
		
		try {
			props.load(Application.class.getClassLoader().getResourceAsStream(path != null ? path : "application.properties"));
		} catch (Throwable t) {
			log.severe("Error loading properties file " + t);
			t.printStackTrace();
			return;
		}

		messageQueueSize = Integer.parseInt(props.getProperty("chat.message.queue.size"));
		port = Integer.parseInt(props.getProperty("chat.port"));
		chatMessageQueueTail = Integer.parseInt(props.getProperty("chat.message.queue.tail"));
		commandClassList = props.getProperty("chat.manager.command.classes");
		bufferSize = Integer.parseInt(props.getProperty("chat.user.buffer.size"));

	}

	private void init() {

		commandManager = new CommandManager(this, new UserCommand[0], commandClassList);

		messageBank = new MessagesBank(messageQueueSize, chatMessageQueueTail);
		messageSeq = 0;

		try {
			selector = Selector.open();

			server = ServerSocketChannel.open();
			server.configureBlocking(false);
			server.register(selector, SelectionKey.OP_ACCEPT);
			server.bind(new InetSocketAddress(port));
		} catch (IOException e) {
			throw new RuntimeException("Cannot open selector");
		}
		
	}

}
