package org.constantinre.chat.server;

import org.constantinre.chat.Message;
import org.constantinre.chat.server.exception.MessageTooOldException;

import java.util.*;
import java.util.logging.Logger;

public class MessagesBank {

	private static Logger log = Logger.getLogger(MessagesBank.class.getName());

	private List<Message> messages;
	private int capacity;
	private int tail;
	
	public MessagesBank(int capacity, int tail) {
		if (capacity < 1) {
			throw new IllegalArgumentException("Capacity < 1");
		}

		this.capacity = capacity;
		this.tail = tail;
		this.messages = new ArrayList<>(capacity);
	}
	
	public boolean isEmpty() {
		return messages.isEmpty();
	}
	
	public boolean isFull() {
		return messages.size() >= capacity;
	}
	
	public void push(Message element) {
		if (isFull()) {
			throw new IllegalStateException("Queue is full");
		}
		
		messages.add(element);
	}
	
	public int size() {
		return messages.size();
	}

	public int searchBySequence(long sequence) {
		Message toFind = new Message(null, null, null);
		toFind.setSequence(sequence);

		return Collections.binarySearch(messages, toFind, Comparator.comparingLong(Message::getSequence));
	}

	public Message get(int position) {
		if (position < 0 || position >= size()) {
			throw new IllegalArgumentException("Position not in [0, size)");
		}
		
		return messages.get(position);
	}

	public void compactMessages(long minKnownMessage) throws MessageTooOldException {
		log.info("Compacting messages (" + messages.size() + ")");

		int found = searchBySequence(minKnownMessage);

		if (found < 0) {
			// next message for slowest user not found = some users too slow
			throw new MessageTooOldException("Message not found");

		} else if (messages.size() - found > tail) {
			// next message for slowest user appear befor tail begin = tail is safe
			List<Message> newList = new ArrayList<>(capacity);
			newList.addAll(messages.subList(found, messages.size()));
			messages = newList;

		} else {
			// next message for slowest user appear after tail begin = truncate messages at tail begin
			List<Message> newList = new ArrayList<>(capacity);
			newList.addAll(messages.subList(messages.size() - tail, messages.size()));
			messages = newList;
		}

		log.info("Compacting messages complete (" + messages.size() + ") ");
	}

}
